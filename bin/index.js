#! /usr/bin/env node

var fs = require('fs');
const { off } = require('process');
const { string, argv } = require("yargs");
const chalk = require('chalk');

var filename = "./" + argv._[0];
var readStream = fs.createReadStream(filename)

readStream.on('open', function () {
    // This just pipes the read stream to the response object (which goes to the client)
    // readStream.pipe(res);
});

readStream.on("data", function (data) {
    var chunk = data.toString();
    var array = chunk.split("\n")
    var counter = 0

    for(var i = 0; i < array.length; i++)
    {
        // Controllo sull'ampiezza delle colonne
        if(array[i].length > 80)
        {
            console.log("\n" + chalk.bgYellow("  ") + " Warning:")
            console.log("\tAt line: " + (i+1))
            console.log("\tError: Column exceeds max lenght (80 chars)")
            console.log("\tLine: " + chalk.gray(array[i]))
            counter ++;
        }   
            

        // Controllo sugli operatori e sulle virgole
        for(var j = 0; j < array[i].length; j++)
        {
            if(array[i].charAt(j) == '+' || array[i].charAt(j) == '-' || array[i].charAt(j) == '*' || array[i].charAt(j) == '/')
            {
                if(array[i].charAt(j-1) != ' ' || array[i].charAt(j+1) != ' ')
                    {
                        console.log("\n" + chalk.bgYellow("  ") + " Warning:")
                        console.log("\tAt line: " + (i+1) + " at char: " + (j+1))
                        console.log("\tError: Operator not correctly formatted.") 
                        console.log("\tLine: " + chalk.gray(array[i]))
                        counter ++;
                    }
            }
            else if (array[i].charAt(j) == ',')
            {
                if(array[i].charAt(j-1) == ' ' || array[i].charAt(j+1) != ' ')
                    {
                        console.log("\n" + chalk.bgYellow("  ") + " Warning:")
                        console.log("\tAt line: " + (i+1) + " at char: " + (j+1))
                        console.log("\tError: Comma not correctly formatted.")
                        console.log("\tLine: " + chalk.gray(array[i]))
                        counter ++;
                    }
            }
        }
    }    
    console.log("\n\nFound " + chalk.yellow.bold(counter) + " warnings.\n\n")
});

    // This catches any errors that happen while creating the readable stream (usually invalid names)
readStream.on('error', function(err) {
    // res.end(err);
});